"use strict"

Yoi = require "yoi"

module.exports = ->
  session = test.owners[0]
  tasks = []
  tasks.push _create(provider, session) for provider in test.providers
  tasks.push _update(test.providers[0], session)
  tasks.push _get(test.providers[0], session)
  tasks

# CRUD
# ====================================================================
_create = (provider, session) -> ->
  Yoi.Test "POST", "api/provider", provider, _setHeader(session), "[PROVIDER] Creado '#{provider.name}'.", 200, (response) ->
    provider.id = response.provider.id

_update = (provider, session) -> ->
  provider = _updateData provider.id
  Yoi.Test "PUT", "api/provider", provider, _setHeader(session), "[PROVIDER] Modificado '#{provider.name}'."

_get = (provider, session) -> ->
  provider = _updateData provider.id
  Yoi.Test "GET", "api/provider", provider, _setHeader(session), "[PROVIDER] Obtenido el '#{provider.name}'."

# Private methods
# ====================================================================
_setHeader = (user) ->
  data =
    "user-agent"    : user.user_agent
    "authorization" : user.token

_updateData = (id) ->
  provider =
    id          : id
    name        : test.providers[0].name + " [UPDATE]"
    description : "Es uno de los mejores bares de pintxos que te puedes encontrar en Bilbao"
  provider
