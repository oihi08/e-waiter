"use strict"

Yoi = require "yoi"

module.exports = ->
  session = test.owners[0]
  tasks = []
  tasks.push _create(product, session) for product in test.products
  tasks.push _update(test.products[0], session)
  tasks.push _listProductProvider(test.clients[0], test.providers[0])
  tasks

# CRUD
# ====================================================================
_create = (product, session) -> ->
  Yoi.Test "POST", "api/product", product, _setHeader(session), "[PRODUCT] Creado '#{product.name}'.", 200, (response) ->
    product.id = response.product.id

_update = (product, session) -> ->
  data = _updateData product
  Yoi.Test "PUT", "api/product", data, _setHeader(session), "[PRODUCT] Modificar '#{product.name}'.", 200

_listProductProvider = (session, provider) -> ->
  data = provider: provider.id
  Yoi.Test "GET", "api/products", data, _setHeader(session), "[PRODUCT] Obtener la lista de productos de '#{provider.name}'.", 200, (response) ->
    console.log response


# Private methods
# ====================================================================
_setHeader = (user) ->
  data =
    "user-agent"    : user.user_agent
    "authorization" : user.token

_updateData = (product) ->
  data =
    id          : product.id
    name        : product.name + " vegetariana"
    description : "Sin carne ni pescado"
  data
