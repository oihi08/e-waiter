"use strict"

Yoi = require "yoi"

module.exports = ->
  session = test.clients[0]
  tasks = []
  tasks.push _create(bill, test.orders[0], session) for bill in test.bills
  # tasks.push _getOrderBill(test.orders[0], session)
  tasks

# CRUD
# ====================================================================
_create = (bill, order, session) -> ->
  data =
    total : bill.total
    order : order.id
  Yoi.Test "POST", "api/bill", data, _setHeader(session), "[BILL] Creada la factura con yn total de '#{bill.total}'.", 200, (response) ->
    bill.id = response.bill.id

_getOrderBill = (bill, session) -> ->
  Yoi.Test "GET", "api/bill", bill, _setHeader(session), "[BILL] Obtener la factura de una pedido .", 200, (response) ->
    bill.id = response.bill.id


# Private methods
# ====================================================================
_setHeader = (user) ->
  data =
    "user-agent"    : user.user_agent
    "authorization" : user.token

