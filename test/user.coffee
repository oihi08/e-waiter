"use strict"

Yoi = require "yoi"

module.exports = ->
  tasks = []

  tasks.push _signupOwner(owner) for owner in test.owners
  tasks.push _loginOwner(owner) for owner in test.owners
  tasks.push _signupClient(client) for client in test.clients
  tasks.push _loginClient(client) for client in test.clients

  tasks

# CRUD
# ====================================================================
_signupOwner = (owner) -> ->
  Yoi.Test "POST", "api/signup/owner", owner, _setHeader(owner), "[SESSION] Registrar un OWNER #{owner.mail}.", 409

_signupClient = (client) -> ->
  Yoi.Test "POST", "api/signup", client, _setHeader(client), "[SESSION] Registrar un CLIENT #{client.mail}.", 409

_loginOwner = (owner) -> ->
  promise = new Yoi.Hope.Promise()
  Yoi.Test "POST", "api/login/owner", owner, _setHeader(owner), "[SESSION] Loguear a #{owner.mail}", 200, (response) ->
    owner.id = response.id
    owner.token = response.token
    owner.appnima_id = response.appnima_id
    promise.done null, response
  promise

_loginClient = (owner) -> ->
  promise = new Yoi.Hope.Promise()
  Yoi.Test "POST", "api/login", owner, _setHeader(owner), "[SESSION] Loguear a #{owner.mail}", 200, (response) ->
    owner.id = response.id
    owner.token = response.token
    owner.appnima_id = response.appnima_id
    promise.done null, response
  promise


# Private methods
# ====================================================================
_setHeader = (user) ->
  "user-agent": "Mozilla/9.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko)"
