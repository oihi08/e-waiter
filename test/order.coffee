"use strict"

Yoi = require "yoi"

module.exports = ->
  client = test.clients[0]
  client2 = test.clients[1]
  tasks = []
  tasks.push _create(test.orders[0], client, test.providers[0])
  tasks.push _create(test.orders[1], client2, test.providers[1])
  tasks.push _create(test.orders[2], client, test.providers[2])
  tasks.push _create(test.orders[3], client2, test.providers[0])
  tasks.push _create(test.orders[4], client, test.providers[2])

  tasks.push _update(test.orders[0], client)
  tasks

# CRUD
# ====================================================================
_create = (order, client, provider) -> ->
  order = _getProducts order
  order.provider = provider.id
  Yoi.Test "POST", "api/order", order, _setHeader(client), "[ORDER] Creado un nuevo pedido.", 200, (response) ->
    order.id = response.order.id

_update = (order, client) -> ->
  attributes = {}
  attributes.id = order.id
  attributes.state = 1
  Yoi.Test "PUT", "api/order", attributes, _setHeader(client), "[ORDER] Modificado un pedido.", 200


# Private methods
# ====================================================================
_setHeader = (user) ->
  data =
    "user-agent"    : user.user_agent
    "authorization" : user.token

_getProducts = (order) ->
  count = 0
  for list in order.list
    product = test.products[count]
    list.product = product.id
    count += 1
  order.list = JSON.stringify(order.list)
  order
