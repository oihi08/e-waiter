"use strict"
Yoi   = require "yoi"
User  = require "./model/user"


module.exports = (rest) ->
  promise = new Yoi.Hope.Promise()
  token = rest.session
  if token?
    User.findOne("appnima.access_token": token).exec (error, user) ->
      if error? or not user?
        do rest.unauthorized
      else
        promise.done error, user
  else
    do rest.unauthorized

  promise
