"use strict"
Yoi    = require("yoi")
Schema = Yoi.Mongoose.Schema
db     = Yoi.Mongo.connections.primary


Product = new Schema
  provider    : type: Schema.ObjectId, ref: "Provider"
  name        : type: String
  description : type: String
  image       : type: String
  price       : type: String
  ref         : type: Number # Identifier
  type        : type: Number # Drink, food
  category    : type: Number # Dessert, menu, rice, salad...
  created_at  : type: Date, default : Date.now

Product.statics.register = (attributes) ->
  promise = new Yoi.Hope.Promise()
  product = db.model "Product", Product
  new product(attributes).save (error, result) ->
    promise.done error, result
  promise

Product.statics.search = (query={}, limit=0) ->
  promise = new Yoi.Hope.Promise()
  @find(query).limit(limit).exec (error, value) ->
    if limit is 1
      error = code: 404, message: "Product not found." if value.length is 0
      value = value[0] if value.length isnt 0
    promise.done error, value
  promise

Product.statics.searchOne = (query) ->
  promise = new Yoi.Hope.Promise()
  @search(query, 1).then (error, result) ->
    error = true unless result
    promise.done error, result
  promise

Product.statics.updateAttributes = (id, attributes) ->
  promise = new Yoi.Hope.Promise()
  @findByIdAndUpdate id, attributes, (error, result) ->
    promise.done error, result
  promise

Product.statics.delete = (id) ->
  promise = new Yoi.Hope.Promise()
  @findByIdAndRemove id, (error, result) ->
    if not result?
      promise.done {code: 404, message: "Product not found."}, null
    else
      promise.done error, result
  promise

Product.methods.parse = ->
  id          : @_id
  provider    : @provider
  name        : @name
  description : @description
  image       : @image
  price       : @price
  category    : @category
  ref         : @ref
  created_at  : @created_at

exports = module.exports = db.model "Product", Product
