"use strict"
Yoi    = require("yoi")
Schema = Yoi.Mongoose.Schema
db     = Yoi.Mongo.connections.primary


Promotion = new Schema
  provider    : type: Schema.ObjectId, ref: "Provider"
  product     : type: Schema.ObjectId, ref: "Product"
  menu        : type: Schema.ObjectId, ref: "Menu"
  name        : type: String
  description : type: String
  price       : type: String
  finished_at : type: Date
  created_at  : type: Date, default : Date.now


Promotion.statics.register = (attributes) ->
  promise = new Yoi.Hope.Promise()
  promotion = db.model "Promotion", Promotion
  new promotion(attributes).save (error, result) ->
    promise.done error, result
  promise

exports = module.exports = db.model "Promotion", Promotion
