"use strict"
Yoi    = require("yoi")
Schema = Yoi.Mongoose.Schema
db     = Yoi.Mongo.connections.primary
CONST  = require "../constants"

Order = new Schema
  provider    : type: Schema.ObjectId, ref: "Provider"
  user        : type: Schema.ObjectId, ref: "User"
  list        : [{
    product  : type: Schema.ObjectId, ref: "Product"
    quantity : type: Number
  }]
  table       : type: Number
  state       : type: Number, default: CONST.ORDER.STATE.PENDING
  stimated_at : type: Date
  comments    : type: String
  created_at  : type: Date, default : Date.now

Order.statics.register = (attributes) ->
  promise = new Yoi.Hope.Promise()
  order = db.model "Order", Order
  new order(attributes).save (error, value) ->
    promise.done error, value
  promise

Order.statics.search = (query={}, limit=0) ->
  promise = new Yoi.Hope.Promise()
  @find(query).limit(limit).exec (error, value) ->
    if limit is 1
      error = code: 404, message: "Order not found." if value.length is 0
      value = value[0] if value.length isnt 0
    promise.done error, value
  promise

Order.statics.searchOne = (query) ->
  promise = new Yoi.Hope.Promise()
  @search(query, 1).then (error, result) ->
    if not result or result.length is 0
      error = code: 404, message: "Order not found."
      promise.done error, null
    else
      promise.done error, result.parse()
  promise

Order.statics.updateAttributes = (filter, attributes) ->
  promise = new Yoi.Hope.Promise()
  @findOneAndUpdate filter, attributes, (error, order) ->
    if not order?
      promise.done {code: 404, message: "Order not found."}, null
    else
      promise.done error, order
  promise

Order.statics.delete = (filter) ->
  promise = new Yoi.Hope.Promise()
  @findByIdAndRemove id, (error, result) ->
    if not result?
      promise.done {code: 404, message: "Order not found."}, null
    else
      promise.done error, result
  promise

Order.methods.parse = ->
  id          : @_id
  provider    : @provider
  user        : @user
  list        : @list
  table       : @table
  comments    : @comments
  state       : @state
  stimated_at : @stimated_at
  created_at  : @created_at

exports = module.exports = db.model "Order", Order
