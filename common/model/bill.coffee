"use strict"
Yoi    = require("yoi")
Schema = Yoi.Mongoose.Schema
db     = Yoi.Mongo.connections.primary


Bill = new Schema
  provider    : type: Schema.ObjectId, ref: "Provider"
  order       : type: Schema.ObjectId, ref: "Order"
  user        : type: Schema.ObjectId, ref: "User"
  total       : type: Number
  created_at  : type: Date, default : Date.now

Bill.statics.register = (attributes) ->
  promise = new Yoi.Hope.Promise()
  bill = db.model "Bill", Bill
  new bill(attributes).save (error, value) ->
    promise.done error, value
  promise

Bill.statics.search = (query={}, limit=0) ->
  promise = new Yoi.Hope.Promise()
  @find(query).limit(limit).exec (error, value) ->
    if limit is 1
      error = code: 404, message: "Bill not found." if value.length is 0
      value = value[0] if value.length isnt 0
    promise.done error, value
  promise

Bill.statics.searchOne = (query) ->
  promise = new Yoi.Hope.Promise()
  @search(query, 1).then (error, result) ->
    if not result or result.length is 0
      error = code: 404, message: "Bill not found."
      promise.done error, null
    else
      promise.done error, result.parse()
  promise

Bill.statics.updateAttributes = (filter, attributes) ->
  promise = new Yoi.Hope.Promise()
  @findOneAndUpdate filter, attributes, (error, order) ->
    if not order?
      promise.done {code: 404, message: "Bill not found."}, null
    else
      promise.done error, order
  promise

Bill.statics.delete = (filter) ->
  promise = new Yoi.Hope.Promise()
  @findByIdAndRemove id, (error, result) ->
    if not result?
      promise.done {code: 404, message: "Bill not found."}, null
    else
      promise.done error, result
  promise

Bill.methods.parse = ->
  id         : @_id
  provider   : @provider
  order      : @order
  user       : @user
  total      : @total
  created_at : @created_at

exports = module.exports = db.model "Bill", Bill
