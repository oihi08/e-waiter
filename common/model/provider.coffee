"use strict"
Yoi    = require("yoi")
Schema = Yoi.Mongoose.Schema
db     = Yoi.Mongo.connections.primary

Provider = new Schema
  owner       : type: Schema.ObjectId, ref: "User"
  name        : type: String
  description : type: String
  address     : type: String
  location    : type: String
  postal_code : type: String
  phone       : type: Number
  site        : type: String
  email       : type: String
  image       : type: String
  timetable   : type: String
  position    : type: [Number], index: "2dsphere", default: [0,0]
  created_at  : type: Date, default : Date.now

Provider.statics.register = (attributes) ->
  promise = new Yoi.Hope.Promise()
  provider = db.model "Provider", Provider
  new provider(attributes).save (error, value) ->
    promise.done error, value
  promise

Provider.statics.search = (query={}, limit=0) ->
  promise = new Yoi.Hope.Promise()
  @find(query).limit(limit).exec (error, value) ->
    if limit is 1
      error = code: 404, message: "Provider not found." if value.length is 0
      value = value[0] if value.length isnt 0
    promise.done error, value
  promise

Provider.statics.searchOne = (query) ->
  promise = new Yoi.Hope.Promise()
  @search(query, 1).then (error, result) ->
    if not result or result.length is 0
      error = code: 404, message: "Provider not found."
      promise.done error, null
    else
      promise.done error, result.parse()
  promise

Provider.statics.updateAttributes = (filter, attributes) ->
  promise = new Yoi.Hope.Promise()
  @findOneAndUpdate filter, attributes, (error, provider) ->
    if not provider?
      promise.done {code: 404, message: "Provider not found."}, null
    else
      promise.done error, provider
  promise

Provider.methods.parse = ->
  id          : @_id
  owner       : @owner
  name        : @name
  description : @description
  address     : @address
  location    : @location
  postal_code : @postal_code
  phone       : @phone
  site        : @site
  email       : @email
  image       : @image
  timetable   : @timetable
  position    : @position

exports = module.exports = db.model "Provider", Provider
