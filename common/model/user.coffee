"use strict"
Yoi    = require("yoi")
Schema = Yoi.Mongoose.Schema
db     = Yoi.Mongo.connections.primary
CONST  = require "../constants"


User = new Schema
  role       : type: Number, default: CONST.USER.ROLE.CLIENT
  likes      : [type: Schema.ObjectId, ref: "Provider"]
  appnima    : {
    id            : type: String
    mail          : type: String
    name          : type: String
    avatar        : type: String
    access_token  : type: String
    refresh_token : type: String
    expire        : type: Date
    password      : type: String
    phone         : type: String
  }
  created_at      : type: Date, default : Date.now

User.statics.register = (appnima) ->
  promise = new Yoi.Hope.Promise()
  user = db.model "User", User
  attributes =
    appnima: appnima
    role: appnima.role
  new user(attributes).save (error, value) ->
    promise.done error, value
  promise

User.statics.search = (query={}, limit=0) ->
  promise = new Yoi.Hope.Promise()
  @find(query).limit(limit).exec (error, value) ->
    if limit is 1
      error = code: 404, message: "User not found." if value.length is 0
      value = value[0] if value.length isnt 0
    promise.done error, value
  promise

User.statics.searchOne = (query) ->
  promise = new Yoi.Hope.Promise()
  @search(query, 1).then (error, result) ->
    if not result or result.length is 0
      error = code: 404, message: "User not found."
      promise.done error, null
    else
      promise.done error, result.parse()
  promise

User.statics.updateToken = (appnima) ->
  promise = new Yoi.Hope.Promise()
  @findOneAndUpdate "appnima.id" : appnima.id, {appnima: appnima}, (error, result) ->
    if not result?
      error = code: 404, message: "User not found."
    promise.done error, result
  promise

User.methods.isAuthorized = ->
  promise = new Yoi.Hope.Promise()
  if @role is CONST.USER.ROLE.OWNER
    promise.done null, @
  else
    error = code: 401, message: "Permission denied."
    promise.done error, null
  promise


User.methods.parse = ->
  id         : @_id
  mail       : @appnima.mail
  name       : @appnima.name
  avatar     : @appnima.avatar
  phone      : @appnima.phone
  token      : @appnima.access_token
  appnima_id : @appnima.id
  expire     : @appnima.expire
  role       : @role
  likes      : @likes

exports = module.exports = db.model "User", User
