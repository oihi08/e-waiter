"use strict"
Yoi    = require("yoi")
Schema = Yoi.Mongoose.Schema
db     = Yoi.Mongo.connections.primary


Menu = new Schema
  provider    : type: Schema.ObjectId, ref: "Provider"
  user        : type: Schema.ObjectId, ref: "User"
  products    : [type: Schema.ObjectId, ref: "Product"]
  name        : type: String
  description : type: String
  price       : type: String
  created_at  : type: Date, default : Date.now


Menu.statics.register = (attributes) ->
  promise = new Yoi.Hope.Promise()
  menu = db.model "Menu", Menu
  new menu(attributes).save (error, result) ->
    promise.done error, result
  promise

exports = module.exports = db.model "Menu", Menu
