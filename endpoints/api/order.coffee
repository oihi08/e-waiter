"use strict"
Yoi       = require "yoi"
Authorize = require "../../common/authorize"
Order     = require "../../common/model/order"

module.exports = (server) ->
  ###
    Get an order.
    @method GET
    @param  {id} Id of order
  ###
  server.get "/api/order", (request, response) ->
    rest = new Yoi.Rest request, response
    Authorize(rest).then (error, session) ->
      Order.searchOne(_id: rest.parameter "id").then (error, order) ->
        if error?
          rest.exception error.code, error.message
        else
          rest.run order: order
  ###
    Create an order.
    @method POST
    @param  {ID} ID of provider
    @param  {Array} Array of list object of products
    @param  {number} Table of provider
    @param  {date} Date of stimation of served order
  ###
  server.post "/api/order", (request, response) ->
    rest = new Yoi.Rest request, response
    rest.required ["provider"]
    Authorize(rest).then (error, session) ->
      Order.register(_getParameters(rest, session)).then (error, order) ->
        if error?
          rest.exception error.code, error.message
        else
          rest.run order: order.parse()
  ###
    Update an order.
    @method POST
    @param  {ID} ID of order
    @param  {ID} ID of provider
    @param  {Array} Array of list object of products
    @param  {number} Table of provider
    @param  {date} Date of stimation of served order
  ###
  server.put "/api/order", (request, response) ->
    rest = new Yoi.Rest request, response
    rest.required ["id"]
    Authorize(rest).then (error, session) ->
      query = _id: rest.parameter "id"
      Order.updateAttributes(query, _getParameters(rest)).then (error, order) ->
        if error?
          rest.exception error.code, error.message
        else
          rest.run order: order.parse()

_getParameters = (rest, user=null) ->
  parameters = {}
  parameters.user = user if user
  parameters.provider = rest.parameter "provider" if rest.parameter "provider"
  parameters.list = JSON.parse(rest.parameter("list")) if rest.parameter "list"
  parameters.table = rest.parameter "table" if rest.parameter "table"
  parameters.state = rest.parameter "state" if rest.parameter "state"
  parameters.stimated_at = rest.parameter "stimated_at" if rest.parameter "stimated_at"
  parameters
