"use strict"
Yoi       = require "yoi"
Authorize = require "../../common/authorize"
Bill      = require "../../common/model/bill"
Order     = require "../../common/model/order"

module.exports = (server) ->
  ###
    Get an bill.
    @method GET
    @param  {ID} ID of bill
  ###
  server.get "/api/bill", (request, response) ->
    rest = new Yoi.Rest request, response
    rest.required ["id"]
    Authorize(rest).then (error, session) ->
      Bill.searchOne(_id: rest.parameter "id").then (error, bill) ->
        if error?
          rest.exception error.code, error.message
        else
          rest.run bill: bill
  ###
    Create an bill.
    @method POST
    @param  {ID} ID of provider
    @param  {ID} ID of order
    @param  {Number} Total of bill
  ###
  server.post "/api/bill", (request, response) ->
    rest = new Yoi.Rest request, response
    rest.required ["order", "total"]
    Authorize(rest).then (error, session) ->
      Yoi.Hope.shield([ ->
        Order.searchOne _id: rest.parameter "order"
      , (error, order) ->
        parameters =
          order: order.id
          provider: order.provider
          total: rest.parameter "total"
          user: session
        Bill.register parameters
      ]).then (error, bill) ->
        if error?
          rest.exception error.code, error.message
        else
          rest.run bill: bill.parse()
  ###
    Update an bill.
    @method PUT
    @param  {ID} ID of bill
    @param  {number} total
  ###
  server.put "/api/order", (request, response) ->
    rest = new Yoi.Rest request, response
    rest.required ["id"]
    Authorize(rest).then (error, session) ->
      query = _id: rest.parameter "id"
      Bill.updateAttributes(query, total: rest.parameter "total").then (error, bill) ->
        if error?
          rest.exception error.code, error.message
        else
          rest.run bill: bill.parse()

