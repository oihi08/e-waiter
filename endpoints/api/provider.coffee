"use strict"
Yoi       = require "yoi"
Authorize = require "../../common/authorize"
Provider  = require "../../common/model/provider"

module.exports = (server) ->
  ###
    Get a provider.
    @method GET
    @param  {id} Id of provider
  ###
  server.get "/api/provider", (request, response) ->
    rest = new Yoi.Rest request, response
    rest.required ["name"]
    Authorize(rest).then (error, session) ->
      Yoi.Hope.shield([->
        session.isAuthorized()
      , (error, session) ->
        Provider.searchOne _id: rest.parameter "id"
      ]).then (error, provider) ->
        if error?
          rest.exception error.code, error.message
        else
          rest.run provider: provider
  ###
    Create a provider.
    @method POST
    @param  {string} Name of provider
    @param  {object} Description of provider
    @param  {string} Address of provider
    @param  {string} Location of provider
    @param  {string} Postal code of provider
    @param  {number} Phone of provider
    @param  {String} Site of provider
    @param  {String} Email of provider
    @param  {String} Avatar of provider
    @param  {string} Timetable of provider
  ###
  server.post "/api/provider", (request, response) ->
    rest = new Yoi.Rest request, response
    rest.required ["name"]
    Authorize(rest).then (error, session) ->
      Yoi.Hope.shield([->
        session.isAuthorized()
      , (error, session) ->
        Provider.register _getParameters(rest, session)
      ]).then (error, provider) ->
        if error?
          rest.exception error.code, error.message
        else
          rest.run provider: provider.parse()

  ###
    Modify provider data.
    @method PUT
    @param  {string} ID of provider
    @param  {string} Name of provider
    @param  {object} Description of provider
    @param  {string} Address of provider
    @param  {string} Location of provider
    @param  {string} Postal code of provider
    @param  {number} Phone of provider
    @param  {String} Site of provider
    @param  {String} Email of provider
    @param  {String} Avatar of provider
    @param  {string} Timetable of provider
  ###
  server.put "/api/provider", (request, response) ->
    rest = new Yoi.Rest request, response
    rest.required ["id"]
    Authorize(rest).then (error, session) ->
      filter =
        _id   : rest.parameter "id"
        owner : session._id
      Provider.updateAttributes(filter, _getParameters(rest)).then (error, provider) ->
        if error?
          rest.exception error.code, error.message
        else
          rest.run provider: provider.parse()

_getParameters = (rest, owner=null) ->
  parameters = {}
  parameters.owner = owner if owner
  parameters.name = rest.parameter "name" if rest.parameter "name"
  parameters.description = rest.parameter "description" if rest.parameter "description"
  parameters.address = rest.parameter "address" if rest.parameter "address"
  parameters.location = rest.parameter "location" if rest.parameter "location"
  parameters.postal_code = rest.parameter "postal_code" if rest.parameter "postal_code"
  parameters.phone = rest.parameter "phone" if rest.parameter "phone"
  parameters.site = rest.parameter "site" if rest.parameter "site"
  parameters.email = rest.parameter "email" if rest.parameter "email"
  parameters.image = rest.parameter "image" if rest.parameter "image"
  parameters.timetable = rest.parameter "timetable" if rest.parameter "timetable"
  parameters.position = rest.parameter "position" if rest.parameter "position"
  parameters
