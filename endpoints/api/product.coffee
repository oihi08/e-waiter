"use strict"
Yoi       = require "yoi"
Authorize = require "../../common/authorize"
Product   = require "../../common/model/product"
Provider  = require "../../common/model/provider"

module.exports = (server) ->

  ###
    Get list of products
    @method GET
    @param  {string} Provider ID
  ###
  server.get "/api/products", (request, response) ->
    rest = new Yoi.Rest request, response
    Authorize(rest).then (error, session) ->
      Product.search(provider: rest.parameter "provider").then (error, products) ->
        if error?
          rest.exception error.code, error.message
        else
          data = (product.parse() for product in products)
          rest.run products: data

  ###
    Provider create a product
    @method POST
    @param  {string}
    @param  {object}
  ###
  server.post "/api/product", (request, response) ->
    rest = new Yoi.Rest request, response
    rest.required ["name"]
    Authorize(rest).then (error, session) ->
      Yoi.Hope.shield([->
        Provider.searchOne owner: session._id
      , (error, provider) ->
        rest.provider = provider.id
        Product.register _getAttributes rest
      ]).then (error, product) ->
        if error?
          rest.exception error.code, error.message
        else
          rest.run product: product.parse()

  ###
    Change info about a product
    @method PUT
    @param  {string}
    @param  {object}
  ###
  server.put "/api/product", (request, response) ->
    rest = new Yoi.Rest request, response
    rest.required ["id"]
    id = rest.parameter "id"
    Authorize(rest).then (error, session) ->
      Yoi.Hope.shield([->
        Provider.searchOne owner: session._id
      , (error, provider) ->
        rest.provider = provider.id
        Product.updateAttributes id, _getAttributes rest
      ]).then (error, product) ->
        if error?
          rest.exception error.code, error.message
        else
          rest.run product: product.parse()


_getAttributes = (rest) ->
  data =
    provider    : rest.provider
    name        : rest.parameter "name"
    description : rest.parameter "description"
    image       : rest.parameter "image"
    type        : rest.parameter "type"
    price       : rest.parameter "price"
  data
