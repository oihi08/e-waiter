Yoi     = require "yoi"
User    = require "../../../common/model/user"
CONST   = require "../../../common/constants"


module.exports = (server) ->
  ###
    Register user OWNER
    @method POST
    @param  {String} User mail
    @param  {String} User password
  ###
  server.post "/api/signup/owner", (request, response, next) ->
    rest = new Yoi.Rest request, response
    rest.required ["mail", "password"]
    Yoi.Hope.shield([->
      Yoi.Appnima.signup request.headers["user-agent"], rest.parameter("mail"), rest.parameter("password")
    , (error, appnima) ->
      appnima.role = 1
      User.register appnima
    ]).then (error, user) ->
      if error?
        rest.exception error.code, error.message
      else
        rest.run user

    ###
    Login user OWNER
    @method POST
    @param  {String} User mail
    @param  {String} User password
  ###
  server.post "/api/login/owner", (request, response, next) ->
    rest = new Yoi.Rest request, response
    rest.required ["mail", "password"]
    user_agent = request.headers["user-agent"]
    appnima = null
    Yoi.Hope.shield([->
      Yoi.Appnima.login user_agent, rest.parameter("mail"), rest.parameter("password")
    , (error, result) ->
      appnima = result
      User.searchOne "appnima.id": appnima.id
    , (error, user) ->
      _checkLogin user, 1
    ]).then (error, user) ->
      if error?
        rest.exception error.code, error.message
      else
        rest.run user

  ###
    Register user CLIENT
    @method POST
    @param  {String} User mail
    @param  {String} User password
  ###
  server.post "/api/signup", (request, response, next) ->
    rest = new Yoi.Rest request, response
    rest.required ["mail", "password"]
    Yoi.Hope.shield([->
      Yoi.Appnima.signup request.headers["user-agent"], rest.parameter("mail"), rest.parameter("password")
    , (error, appnima) ->
      appnima.role = 0
      User.register appnima
    ]).then (error, user) ->
      if error?
        rest.exception error.code, error.message
      else
        rest.run user

  ###
    Login users CLIENT
    @method POST
    @param  {String} User mail
    @param  {String} User password
  ###
  server.post "/api/login", (request, response, next) ->
    rest = new Yoi.Rest request, response
    rest.required ["mail", "password"]
    user_agent = request.headers["user-agent"]
    appnima = null
    Yoi.Hope.shield([->
      Yoi.Appnima.login user_agent, rest.parameter("mail"), rest.parameter("password")
    , (error, result) ->
      appnima = result
      User.searchOne "appnima.id": appnima.id
    , (error, user) ->
      _checkLogin user, 0
    ]).then (error, user) ->
      if error?
        rest.exception error.code, error.message
      else
        rest.run user

_checkLogin = (user, role) ->
  promise = new Yoi.Hope.Promise()
  if user.role is role
    promise.done null, user
  else
    error = code: 409, message: "The user has not permissions."
    promise.done error, null
  promise
