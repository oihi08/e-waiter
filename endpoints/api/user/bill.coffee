"use strict"
Yoi       = require "yoi"
Authorize = require "../../../common/authorize"
Bill      = require "../../../common/model/bill"

module.exports = (server) ->
  ###
    Get an user bills.
    @method GET
    @param  {ID} ID of bill
  ###
  server.get "/api/user/bills", (request, response) ->
    rest = new Yoi.Rest request, response
    Authorize(rest).then (error, session) ->
      query = user: session
      Bill.search(user: session).then (error, bills) ->
        if error?
          rest.exception error.code, error.message
        else
          data = (bill.parse() for bill in bills)
          rest.run bills: data
